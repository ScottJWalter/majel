import importlib
import pkgutil
import signal
import sys

from argparse import ArgumentParser
from pathlib import Path

from pyaudio import PyAudio

from . import __version__
from .audio.recorder import Recorder
from .helpers.audio import pyaudio
from .listener import KeyListener
from .logger import Loggable
from .managers import Manager
from .voices.base import voice


class Majel(Loggable):
    """
    The primary daemon that runs indefinitely waiting for a KeyboardInterrupt.
    This class spawns a whole bunch of threads, one for each handler + one for
    the listener and one for the audio recorder.  All of these threads remain
    attached to this one though, so if you kill this process, they all die too.
    """

    def __init__(self):

        parser = ArgumentParser()
        parser.add_argument("--version", action="store_true")
        self.args = parser.parse_args()

        self.manager: Manager
        self.listener: KeyListener
        self.recorder: Recorder

    def __call__(self, audio: PyAudio) -> int:

        if self.args.version:
            sys.stdout.write(f"{__version__}\n")
            return 0

        self.manager = Manager()

        self.listener = KeyListener(
            on_start=self.toggle_recording,
            on_stop=self.manager.stop,
            on_command=self.manager.execute,
        )
        self.recorder = Recorder(
            audio=audio,
            callback=self.handle_recording,
        )

        self.load_plugins()

        self.manager.start()  # Initiates all the Handler threads
        self.listener.start()  # The key listener thread
        self.recorder.start()  # Readies the recording thread

        voice.tts("mayjell is ready")
        self.logger.info("Majel is ready")

        try:
            signal.pause()
        except KeyboardInterrupt:
            self.logger.info("Exiting")
            self.manager.shutdown()
            return 0

    def load_plugins(self) -> None:
        """
        Plugins follow the naming convention `^majel_.*`, and the discovery
        process is as simple as detecting the child classes of the base class,
        so all we need to do is load each plugin module into memory, and we
        should be good to go.
        """

        for __, name, __ in pkgutil.iter_modules():
            if name.startswith("majel_"):
                self.logger.info("Loading plugin: %s", name)
                importlib.import_module(name)

    def toggle_recording(self) -> None:

        if self.recorder.should_record:
            self.logger.debug("Stopping voice recording")
            self.recorder.should_record = False
            return

        self.logger.debug("Starting voice recording")
        self.recorder.should_record = True

    def handle_recording(self, path: Path) -> None:
        text = voice.stt(path)
        self.logger.info('This is what Majel thinks you said: "%s"', text)
        self.manager.execute(command=text)

    @classmethod
    def run(cls):
        with pyaudio() as audio:
            return cls()(audio)
