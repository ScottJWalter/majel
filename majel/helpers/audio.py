from pyaudio import PyAudio

from .noisy import silence


class pyaudio:
    """
    PyAudio is noisy af every time you initialise it, which makes reading the
    log output rather difficult.  The output appears to be being made by the
    C internals, so I can't even redirect the logs with Python's logging
    facility.  Therefore the nuclear option was selected: swallow all stderr
    and stdout for the duration of PyAudio's use.
    """

    def __init__(self):
        self.pyaudio = None

    def __enter__(self) -> PyAudio:
        with silence():
            self.pyaudio = PyAudio()
        return self.pyaudio

    def __exit__(self, *_):
        self.pyaudio.terminate()
