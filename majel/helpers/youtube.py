from pathlib import Path
from typing import Optional
from urllib.parse import quote

import requests

from diskcache import Cache

from ..config import config
from ..logger import Loggable


class YouTube(Loggable):

    SEARCH_URL = (
        "https://www.googleapis.com/youtube/v3/search"
        "?part=snippet"
        "&maxResults=1"
        "&q={query}"
        "&safeSearch=none"
        "&order=relevance"
        "&type=video"
        "&fields=items%2Fid%2FvideoId%2Citems%2Fsnippet%2Ftitle"
        "&key={key}"
    )

    CACHE_FILE = Path.home() / ".cache" / "majel" / "youtube"
    CACHE_TIME = 60 * 60 * 6  # 6 hours

    def __init__(self, api_key: str):
        self.api_key = api_key

    def search(self, query: str, full_screen=False) -> Optional[str]:
        """
        Details here:
          https://developers.google.com/youtube/v3/code_samples/code_snippets
        """

        self.logger.info("Searching YouTube for %s", query)

        with Cache(self.CACHE_FILE.as_posix()) as reference:

            if url := reference.get(query):
                if full_screen:
                    return url.replace("youtube.com", "yout-ube.com")
                return url

            self.logger.info("Cache miss")

            try:
                url = self.SEARCH_URL.format(
                    query=quote(query), key=self.api_key
                )
                response = requests.get(url).json()
                for video in response["items"]:
                    self.logger.info(video["snippet"]["title"])
                video_id = response["items"][0]["id"]["videoId"]
            except (KeyError, IndexError):
                return None

            reference.set(
                query,
                f"https://www.youtube.com/watch?v={video_id}",
                expire=self.CACHE_TIME,
            )

        return self.search(query, full_screen=full_screen)  # Recursive baby!


youtube = YouTube(api_key=config.handlers.youtube.key)
