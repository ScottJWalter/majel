import sys
import wave

from collections import Counter, deque
from pathlib import Path
from threading import Thread
from time import sleep
from typing import Callable

from alsaaudio import Mixer
from pyaudio import PyAudio, get_sample_size, paInt16
from webrtcvad import Vad

from ..logger import Loggable
from .player import notifications


class Recorder(Loggable, Thread):
    """
    A daemon that you can tell to listen to the mic and create audio files by
    setting `.should_record` accordingly.

    NOTE: This works as it is, but PyAudio is kind of a mess as a library.  If
          it gives me any more trouble, I'll have to refactor this whole class
          to use python-sounddevice, which conveniently can also play audio.
    """

    CHUNK = 1024
    FORMAT = paInt16
    CHANNELS = 1
    RATE = 16000

    LOOP_WAIT = 0.5
    OUTPUT_PATH = Path("/") / "tmp" / "majel-recording.wav"
    MAX_RECORDING_TIME = 10  # Seconds

    def __init__(self, audio: PyAudio, callback: Callable):
        super().__init__(daemon=True)
        self.audio = audio
        self.should_record = False
        self.callback = callback
        self.mixer = Mixer()  # Defaults to the master mixer

    def run(self) -> None:
        while True:
            self.logger.debug("Recording status: %s", self.should_record)
            if self.should_record:
                self.acquire()  # Runs its own infinite loop
                self.callback(self.OUTPUT_PATH)
            sleep(self.LOOP_WAIT)

    def acquire(self) -> None:
        """
        This method should only be triggered when `.should_record` is True, and
        will continue until it's False, at which point it pushes everything it
        recorded into a file and returns.
        """

        notifications.acknowledged.play()

        self.logger.info("Acquiring voice sample")

        self.mixer.setmute(1)

        # This can be a file handle if that makes more sense
        with wave.open(str(self.OUTPUT_PATH), "wb") as wf:

            wf.setnchannels(self.CHANNELS)
            wf.setsampwidth(get_sample_size(self.FORMAT))
            wf.setframerate(self.RATE)

            self.logger.info("Voice sample recorded")

            wf.writeframes(self._get_audio())
            wf.close()

        self.mixer.setmute(0)

    def _get_audio(self):

        # TODO: Maybe move these into class constants?
        vad = Vad(1)
        frame_duration_ms = 30
        padding_duration_ms = 990

        num_padding_frames = int(padding_duration_ms / frame_duration_ms)
        ring_buffer = deque(maxlen=num_padding_frames)

        n = int(self.RATE * (30 / 1000.0) * 2)

        stream = self.audio.open(
            format=self.FORMAT,
            channels=self.CHANNELS,
            rate=self.RATE,
            input=True,
            frames_per_buffer=self.CHUNK,
        )

        frames = []
        chunk = b""
        while True:

            chunk += stream.read(self.CHUNK)

            while len(chunk) >= n:

                frame, chunk = chunk[:n], chunk[n:]

                ring_buffer.append(vad.is_speech(frame, self.RATE))

                frames.append(frame)

                self._render_ring_buffer(ring_buffer)

                if len(ring_buffer) < ring_buffer.maxlen:
                    continue

                if True not in ring_buffer:
                    continue

                if Counter(ring_buffer)[False] / ring_buffer.maxlen > 0.9:
                    self.should_record = False

            if not self.should_record:
                self.logger.info("Remainder: %s vs. %s", len(chunk), n)
                break

        stream.stop_stream()
        stream.close()

        return b"".join(frames)

    @staticmethod
    def _render_ring_buffer(buffer: deque):
        sys.stdout.write(str("".join([str(int(_)) for _ in buffer])) + "\n")
        sys.stdout.flush()
