import os
import socket
import sys

from argparse import ArgumentParser
from pathlib import Path


class Controller:

    RUNTIME_DIR = Path(os.getenv("XDG_RUNTIME_DIR", "/tmp")) / "majel"

    def __init__(self) -> None:

        self.socket_path = self.RUNTIME_DIR / "majel.sock"

        self.parser = ArgumentParser()
        self.parser.add_argument("action", type=self._clean_action)

        self.args = self.parser.parse_args()

        self.__session = None

    def __call__(self, *args, **kwargs) -> int:

        try:
            with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as o:
                o.connect(self.socket_path.as_posix())
                o.send(self.args.action.encode())
        except FileNotFoundError:
            sys.stderr.write(
                f"The socket isn't set up at {self.socket_path}.  "
                "Make sure that Majel is running.\n"
            )
            return 1

        return 0

    @staticmethod
    def _clean_action(string):
        if string == "listen":
            return "start"
        return string

    @classmethod
    def run(cls) -> None:
        sys.exit(cls()())
