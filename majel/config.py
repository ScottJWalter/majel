from __future__ import annotations

import platform

from pathlib import Path
from subprocess import PIPE, run
from typing import Generator, Optional, Sequence, Tuple, Type

import yaml

from gi.repository import Gio  # NOQA: PyCharm is wrong
from pydantic import BaseModel, create_model

from .logger import Loggable


class KeyBindings(BaseModel):
    overview: Sequence[str]
    maximise: Sequence[str]
    switch_to_1: Sequence[str]
    switch_to_2: Sequence[str]
    switch_to_3: Sequence[str]
    switch_to_4: Sequence[str]
    switch_to_5: Sequence[str]
    switch_to_6: Sequence[str]
    switch_to_7: Sequence[str]
    switch_to_8: Sequence[str]
    switch_to_9: Sequence[str]


class VoicePreferences(BaseModel):
    tts: str = "mimic"
    stt: str = "vosk"


class Vosk(BaseModel):
    source: Optional[str] = "vosk-model-en-us-0.22"


class Voice(BaseModel):
    preferences = VoicePreferences()
    vosk = Vosk()


class Config(BaseModel):
    @classmethod
    def get_subclass_leaves(cls) -> Generator[Type[Config]]:
        for subclass in cls.__subclasses__():
            yield from subclass.get_subclass_leaves()
            if not subclass.__subclasses__():
                yield subclass


class HandlerConfig(Config):
    pass


class APIConfig(Config):
    pass


class Configuration(Loggable):
    """
    A singleton that does a bunch of work to build itself the first time you
    try to use it.
    """

    CONFIG = Path.home() / ".config" / "majel" / "config.yaml"

    SHELL_BINDINGS = "org.gnome.shell.keybindings"
    WM_BINDINGS = "org.gnome.desktop.wm.keybindings"

    # Map the key names in Gio to values pyautogui will understand
    KEY_MAP = {
        "Super": "winleft",
        "Primary": "ctrlleft",
        "Shift": "shiftleft",
        "Control": "ctrlleft",
        "Alt": "altleft",
        "Page_Up": "pageup",
        "Page_Down": "pagedown",
        "Left": "left",
        "Right": "right",
        "Up": "up",
        "Down": "down",
        "Above_Tab": "`",
        "Escape": "esc",
        "space": " ",
    }

    UNAVAILABLE_KEY_MESSAGE = """\n
        Majel requires a variety of keyboard shortcuts to be set in order to
        work: `toggle-overview`, `maximize`, and `switch-to-workspace-[1-9]`.
        One or more of these hasn't been set on this machine, so Majel will not
        operate properly.  To fix this, run the following (or some variation
        thereof) in a separate shell and restart Majel:

          gsettings set org.gnome.shell.keybindings toggle-overview "['<Super>s']"
          gsettings set org.gnome.desktop.wm.keybindings maximize "['<Super>Page_Up']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Primary>F1']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Primary>F2']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Primary>F3']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Primary>F4']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 "['<Primary>F5']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 "['<Primary>F6']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-7 "['<Primary>F7']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-8 "['<Primary>F8']"
          gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-9 "['<Primary>F9']"
          gsettings set org.gnome.mutter dynamic-workspaces false
          gsettings set org.gnome.desktop.wm.preferences num-workspaces 9
    """  # NOQA: E501

    _shell_keys: Gio.Settings
    _wm_keys: Gio.Settings

    keys: KeyBindings
    installed_apps: Tuple[str, ...]

    voice: Voice
    apis: Config
    handlers: Config

    theme: str

    def __init__(self):

        self._is_setup = False

        # If we want this to be more accurate, we should have a look at psutil
        self.is_slow = platform.machine() != "x86_64"

    def __getattr__(self, item):
        """
        Lazy-load the setup process, so we can safely import config all over
        the place without creating import loops.
        """
        if not self.__getattribute__("_is_setup"):
            self.__getattribute__("_setup")()
        return self.__getattribute__(item)

    def _setup(self) -> None:

        self._setup_keys()
        self._setup_installed_apps()
        self._load_config()
        self.theme = self._get_theme()

    def _load_config(self):

        data = {}
        write_config = False

        if self.CONFIG.exists():

            with self.CONFIG.open() as f:
                data = yaml.safe_load(f)

        else:

            write_config = True
            self.logger.info(
                "The configuration file %s does not exist.  "
                "Proceeding with the defaults",
                self.CONFIG,
            )

        self.voice = Voice(**data.get("voice", {}))
        self.handlers = self._setup_component(
            HandlerConfig,
            data.get("handlers", {}),
        )
        self.apis = self._setup_component(APIConfig, data.get("apis", {}))

        if write_config:
            self._write_config()

    def _setup_component(
        self,
        parent_class: Type[Config],
        data: dict,
    ) -> Config:
        """
        Each plugin has the option to contain a `config.py` that will be
        dynamically loaded here at runtime and attached to the appropriate
        parent configuration class.
        """

        kwargs = {}
        for c in parent_class.get_subclass_leaves():
            self.logger.info("Loading configuration for %s", c.__name__)
            name = c.__name__[:-6].lower()
            kwargs[name] = c(**data.get(name, {}))  # NOQA: PyCharm is wrong.

        class_name = parent_class.__name__.replace("Config", "s")
        return create_model(class_name, **kwargs)()

    def _setup_keys(self):

        self._shell_keys = Gio.Settings(schema=self.SHELL_BINDINGS)
        self._wm_keys = Gio.Settings(schema=self.WM_BINDINGS)

        self.keys = KeyBindings(
            overview=self._capture_key("toggle-overview"),
            maximise=self._capture_key("maximize"),
            switch_to_1=self._capture_key("switch-to-workspace-1"),
            switch_to_2=self._capture_key("switch-to-workspace-2"),
            switch_to_3=self._capture_key("switch-to-workspace-3"),
            switch_to_4=self._capture_key("switch-to-workspace-4"),
            switch_to_5=self._capture_key("switch-to-workspace-5"),
            switch_to_6=self._capture_key("switch-to-workspace-6"),
            switch_to_7=self._capture_key("switch-to-workspace-7"),
            switch_to_8=self._capture_key("switch-to-workspace-8"),
            switch_to_9=self._capture_key("switch-to-workspace-9"),
        )
        self.logger.info("Captured key bindings: %s", self.keys)

        self._check_keys()

    def _capture_key(self, name: str) -> Sequence[str]:

        bindings = self._wm_keys
        if name not in bindings:
            bindings = self._shell_keys

        try:
            gio_name = bindings[name][0]  # NOQA
        except IndexError:
            return ()

        keys = gio_name.replace("<", "").split(">")
        return tuple(self.KEY_MAP.get(k, k).lower() for k in keys)

    def _setup_installed_apps(self) -> None:
        self.installed_apps = tuple(
            sorted({_.get_name().lower() for _ in Gio.app_info_get_all()})
        )
        self.logger.debug("Installed applications: %s", self.installed_apps)

    def _check_keys(self) -> None:
        for key, value in self.keys.dict().items():
            if not value:
                self.logger.error(self.UNAVAILABLE_KEY_MESSAGE)
                return

    def _write_config(self):
        self.logger.info(
            "Writing a default configuration to %s",
            self.CONFIG.absolute().as_posix(),
        )
        with self.CONFIG.open("w") as f:
            yaml.safe_dump(data=self.as_dict(), stream=f, sort_keys=False)

    def as_dict(self):
        return {
            "version": 1,
            "voice": self.voice.dict(),
            "handlers": self.handlers.dict(),
            "apis": self.apis.dict(),
        }

    @staticmethod
    def _get_theme():
        system_theme = run(
            (
                "gsettings",
                "get",
                "org.gnome.desktop.interface",
                "gtk-theme",
            ),
            stdout=PIPE,
        ).stdout.decode()
        if "dark" in system_theme.lower():
            return "dark"
        return "light"


config = Configuration()
