import os
import socket

from pathlib import Path
from shutil import rmtree
from threading import Thread
from typing import Callable

from .logger import Loggable


class KeyListener(Thread, Loggable):
    """
    This whole thing exists because Linux key listening in Python is kinda
    sketchy.  If you're using Xorg you can rely on the `pynput` library to get
    the job done, but if you're on Wayland, there's nothing for you to work
    with outside of talking directly to evdev which requires root privileges.

    So instead, we're leveraging GNOME's keyboard shortcuts to call a script
    which writes to a socket to which this thread listens and performs actions
    based on what it receives.  It's a little convoluted, but at least there's
    now only one way to do this regardless of window manager.

    The key listener "listens" on a UNIX socket for just 3 cases:

    * `start`: Trigger whatever was passed in as `on_start`
    * `stop`:  Trigger whatever was passed in as `on_stop`
    * `an arbitrary command`: Trigger said command

    Note that there's a method on here called `.stop()` which does *not* relate
    to the above but rather is the method you call when you want to stop the
    listener.  It does this by setting `self.should_stop` and then passing
    nothing to the socket which then short-circuits the infinite loop.
    """

    ACTION_START = "start"
    ACTION_STOP = "stop"
    ACTIONS = (ACTION_START, ACTION_STOP)

    PRIORITY = 0
    RUNTIME_DIR = Path(os.getenv("XDG_RUNTIME_DIR", "/tmp")) / "majel"

    def __init__(
        self,
        on_start: Callable,
        on_stop: Callable,
        on_command: Callable,
    ) -> None:

        super().__init__(daemon=True)

        self.should_stop = False
        self.on_start = on_start
        self.on_stop = on_stop
        self.on_command = on_command

        if self.RUNTIME_DIR.exists():
            rmtree(self.RUNTIME_DIR)
        self.RUNTIME_DIR.mkdir(0o700)

        self.socket_path = self.RUNTIME_DIR / "majel.sock"

    def run(self) -> None:

        with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:

            s.bind(self.socket_path.as_posix())
            s.listen()

            while True:

                connection, __ = s.accept()
                action = ""

                with connection:

                    while True:

                        received = connection.recv(1024).decode()
                        action += received
                        if not received:
                            break

                    self.logger.info("Listener received: %s", action)

                    if action == self.ACTION_START:
                        self.on_start()
                    elif action == self.ACTION_STOP:
                        self.on_stop()
                    else:
                        self.on_command(action)

                if self.should_stop:
                    break

        self.socket_path.unlink()

    def stop(self):
        """
        Trigger this when you want to stop the listener.
        """
        self.logger.info("Stopping key listener")
        self.should_stop = True
        with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
            s.connect(self.socket_path.as_posix())
            s.send(b"")
