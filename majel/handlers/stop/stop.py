from ..base import Handler


class OpenHandler(Handler):

    PRIORITY = 1

    def __str__(self):
        return "Stop"

    def can_handle(self, command: str) -> bool:
        return command == "stop"

    def handle(self, command: str) -> None:
        self.manager.stop()
