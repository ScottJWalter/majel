import re

from .....config import config
from ..streamer import StreamerHandler


class PrimeHandler(StreamerHandler):

    PRIORITY = 0.5

    CONFIG = config.handlers.prime
    REQUIRED_CONFIG = ("profile", "is_enabled", "profile", "region")

    WORKSPACE = config.handlers.prime.workspace
    PROFILE_NAME = config.handlers.prime.profile

    # TODO:
    #   Amazon has different URLs for each country.  We'll need a complete list
    #   to do this right.
    START_URLS = {
        "gb": "https://www.amazon.co.uk/gp/video/storefront/",
    }

    WATCH_REGEX = re.compile(r"^watch (?P<title>.*?) on (amazon )?prime$")
    URL_REGEX = re.compile(r"^prime:(?P<url>http.*)$")

    def __str__(self) -> str:
        return "Amazon Prime"

    def get_start_url(self):
        try:
            return self.START_URLS[config.handlers.prime.region]
        except KeyError:
            return self.START_URLS[tuple(self.START_URLS.keys())[0]]
