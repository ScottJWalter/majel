import re

from time import sleep

from ....config import config
from ....exceptions import CantHandleError
from ....helpers.gui import gui, wait_for_pixel
from ....helpers.youtube import youtube
from ..base import ChromiumHandler


class YoutubeHandler(ChromiumHandler):

    PRIORITY = 0.5
    WORKSPACE = config.handlers.youtube.workspace
    CONFIG = config.handlers.youtube
    REQUIRED_CONFIG = ("profile", "is_enabled", "key")
    PROFILE_NAME = CONFIG.profile
    START_URL = "https://youtube.com/"

    HANDLE_REGEX = re.compile(r"^you ?(tube|too?) (?P<title>.*?)$")

    def __init__(self, manager):

        super().__init__(manager=manager)

        self.is_playing = False

    def can_handle(self, command: str) -> bool:
        return bool(self.HANDLE_REGEX.match(command))

    def handle(self, command: str) -> None:

        self.is_playing = True

        query = self.HANDLE_REGEX.match(command).group("title")

        if url := youtube.search(query, full_screen=True):
            self.switch_to_workspace()
            gui.move_out_of_the_way()
            with wait_for_pixel(str(self)):
                self.go_to(url)
                sleep(1)
            gui.click(gui.centre)
            gui.move_out_of_the_way()
            return

        raise CantHandleError(
            f"A search of Youtube for {query} returned up 0 results"
        )
