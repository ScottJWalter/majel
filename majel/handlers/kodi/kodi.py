import re

from shutil import which
from subprocess import PIPE, Popen
from typing import Optional

from ...config import config
from ...exceptions import CantHandleError
from ...helpers.gui import wait_for_pixel
from ...helpers.kodi import kodi
from ..workspace import WorkspaceHandler


class KodiHandler(WorkspaceHandler):

    PRIORITY = 0.2

    CONFIG = config.handlers.kodi
    REQUIRED_CONFIG = ("is_enabled",)

    WORKSPACE = CONFIG.workspace

    # TODO: This should follow the `watch x on kodi` style of the others.
    HANDLE_REGEX = re.compile(
        r"^(kodi|cody)"  # STT is likely to assume "cody"
        r"("
        r"(:(?P<lookup>[^ ]+) (?P<id>\d+))"  # Internally routed
        r"|"
        r"( (?P<title>.+))"  # Spoken request
        r")"
    )

    binary: str
    process: Optional[Popen] = None

    def __init__(self, manager):
        self.connection_is_working = False
        super().__init__(manager)

    @property
    def is_enabled(self) -> bool:

        configured = super().is_enabled

        if configured:
            self.binary = which("kodi")
            if not self.binary:
                self.logger.warning(
                    "Kodi doesn't appear to be installed, so Majel won't try "
                    "to run it."
                )
                return False

        return configured

    def setup(self) -> None:
        with wait_for_pixel(str(self)):
            self.process = Popen(("kodi",), stdout=PIPE, stderr=PIPE)

    def shutdown(self) -> None:
        if self.process:
            self.process.kill()

    def can_handle(self, command: str) -> bool:
        return bool(self.HANDLE_REGEX.match(command))

    def handle(self, command: str) -> None:

        self.logger.info("Attempting to handle %s", command)

        if not self.connection_is_working and not kodi.test_connection():
            raise CantHandleError(
                "While Majel is configured to talk to Kodi, Kodi hasn't "
                "been setup to allow control via HTTP.  Have a look here "
                "for instructions: https://kodi.wiki/view/Web_interface "
                "and be sure to include the username & password you use "
                "when setting up HTTP acces in your "
                "${HOME}/.config/majel/config.yaml file."
            )

        self.connection_is_working = True

        self.switch_to_workspace()

        m = self.HANDLE_REGEX.match(command)

        if m.group("title"):

            self.logger.info("Kodi lookup requested for %s", m.group("title"))
            kodi.play(*kodi.search(m.group("title")))

        else:

            self.logger.info(
                "Kodi direct play request for %s:%s",
                m.group("lookup"),
                m.group("id"),
            )

            kodi.play(m.group("lookup"), int(m.group("id")))

    def stop(self):
        kodi.stop()
