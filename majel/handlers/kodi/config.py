from typing import Optional

from ...config import HandlerConfig


class KodiConfig(HandlerConfig):
    username: str = "kodi"
    password: Optional[str]
    endpoint: str = "http://localhost:8080/jsonrpc"
    workspace: int = 5
    is_enabled: bool = False
