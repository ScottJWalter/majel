from __future__ import annotations

from time import sleep

import pyautogui

from ..config import config
from ..exceptions import ProgrammingError
from ..helpers.gui import wait_for_pixel
from .base import Handler


class WorkspaceHandler(Handler):

    WORKSPACE: int  # Anything from 1 to 9 inclusive
    PIXEL_COORDS = (300, 300)

    def __init__(self, manager):

        if not self.WORKSPACE:
            raise ProgrammingError("A manager must have a defined workspace")

        super().__init__(manager)

        if self.is_enabled:

            self.logger.info("Starting %s", self)

            self.switch_to_workspace()

            with wait_for_pixel(str(self)):
                self.setup()
            self.logger.info("Setup complete")

            self.on_ready()
            self.logger.info("Ready complete")

    def handle(self, command: str) -> None:
        raise NotImplementedError()

    def setup(self) -> None:
        raise NotImplementedError()

    def on_ready(self) -> None:
        pass

    def switch_to_workspace(self) -> None:
        self.logger.info("Moving to workspace %s", self.WORKSPACE)
        hotkey = getattr(config.keys, f"switch_to_{self.WORKSPACE}")
        self.logger.debug("Executing %s", hotkey)
        pyautogui.hotkey(*hotkey)
        if config.is_slow:
            sleep(2)

    def shutdown(self) -> None:
        """
        If the manager won't die along with Majel, this should be triggered on
        the way down to explicitly kill the process.
        """
        self.logger.info("Shutting down %s", self)
