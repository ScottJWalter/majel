#!/bin/bash

# Installing `majel` alone doesn't let you do much, so we install stuff that
# it can control to be more interesting.

yay -S --noconfirm \
  inetutils \
  tk \
  gobject-introspection \
  mesa-vdpau \
  kodi \
  community/chromium-docker \
  firefox \
  mimic \
  majel


# We have to be using Xorg.  Wayland won't work with Majel yet.

sed -i -E 's/^#WaylandEnable/WaylandEnable/' /etc/gdm/custom.conf


# Add a startup script so the user can type `<Super>+Majel+<Enter>`

cat <<EOF > /usr/share/applications/majel.desktop
[Desktop Entry]
Version=1.0
Name=Majel
Comment=Automate your desktop with your voice
Exec=gnome-terminal -e "bash -c '/opt/majel/bin/majel;\$SHELL'"
Icon=utilities-terminal
Terminal=false
Type=Application
Categories=Application;
EOF


# When a user starts an X session, configure the key commands required to get
# Majel running and then create a file in ${HOME}/.config/majel to make sure
# that this only runs once.

cat <<EOF > /usr/local/bin/majel-setup
#!/usr/bin/env bash

if [ -e "${HOME}/.config/majel/autostart-has-run" ]; then
  exit 0
fi

# Keybindings
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/']"

# Keybinding: Listen
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Primary><Shift>F11"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "/opt/majel/bin/majel-controller start"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Majel Start Listening"

# Keybinding: Stop
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Primary><Shift>F12"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "/opt/majel/bin/majel-controller stop"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "Majel Stop Listening"

# Keybindings: Navigation
gsettings set org.gnome.shell.keybindings toggle-overview "['<Super>s']"
gsettings set org.gnome.desktop.wm.keybindings maximize "['<Super>Page_Up']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Primary>F1']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Primary>F2']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Primary>F3']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Primary>F4']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-5 "['<Primary>F5']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-6 "['<Primary>F6']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-7 "['<Primary>F7']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-8 "['<Primary>F8']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-9 "['<Primary>F9']"
gsettings set org.gnome.mutter dynamic-workspaces false
gsettings set org.gnome.desktop.wm.preferences num-workspaces 9

mkdir -p \${HOME}/.config/majel

# Install an extension to kill GNOME's animation for workspace switching
cd /tmp
git clone https://github.com/amalantony/gnome-shell-extension-instant-workspace-switcher.git
cd gnome-shell-extension-instant-workspace-switcher
cp -r instantworkspaceswitcher@amalantony.net/ ${HOME}/.local/share/gnome-shell/extensions
rm -rf /tmp/gnome-shell-extension-instant-workspace-switcher

touch \${HOME}/.config/majel/autostart-has-run
EOF

chmod 755 /usr/local/bin/majel-setup


# We assume that the primary user on this pi has id 1000

cat <<EOF > /etc/xdg/autostart/majel-setup.desktop
[Desktop Entry]
Type=Application
Name=Majel Setup
Exec=/usr/local/bin/majel-setup
NoDisplay=true
EOF


# We're all set.  Reboot to (hopefully) have everything working properly.

reboot
